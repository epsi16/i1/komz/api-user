const express = require('express');
const app = express();
const cors = require('cors');
const axios = require('axios');
const fileUpload = require('express-fileupload');
const FormData = require('form-data');
const { Pool } = require('pg');
const { v4: uuidv4 } = require('uuid');
const config = require('./config');
const { response } = require('express');

app.use(cors());

app.use(express.json());

app.use(fileUpload());

const pool = new Pool({
    user: config.POSTGRESQL_USER,
    host: config.POSTGRESQL_HOST,
    database: config.POSTGRESQL_DATABASE,
    password: config.POSTGRESQL_PASSWORD,
    port: config.POSTGRESQL_PORT
});

app.get('/user/bdd', (req, res) => {

    console.log("[GET] /user/bdd");

    console.log(config.POSTGRESQL_USER);
    console.log(config.POSTGRESQL_HOST);
    console.log(config.POSTGRESQL_DATABASE);
    console.log(config.POSTGRESQL_PASSWORD);
    console.log(config.POSTGRESQL_PORT);
    console.log(config.API_IMAGE_URL);

    res.header('Access-Control-Allow-Origin', '*');

    console.log(pool.connect());

    res.send("ok");
});

app.get('/user/:username', (req, res) => {

    console.log("[GET] /user/" + req.params.username);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query('SELECT *, (SELECT COUNT(*) FROM PUBLIC.subscribe WHERE id_user = u.id) AS count_subscribe_to, (SELECT COUNT(*) FROM PUBLIC.subscribe WHERE id_subscribe = u.id) AS count_subscribe_by FROM PUBLIC.user u WHERE upper(username) LIKE upper($1)', [req.params.username], async(error, result) => {
        if (result.rows.length !== 1) {
            res.status(404).send({ error: "This user doesn't exist" });
        }else{
            res.send({
                'id' : result.rows[0].id,
                'biography' : result.rows[0].biography,
                'boolean_reset_password' : result.rows[0].boolean_reset_password,
                'first_name' : result.rows[0].first_name,
                'last_name' : result.rows[0].last_name,
                'link_image_profile' : result.rows[0].link_image_profile,
                'link_reset_password' : result.rows[0].link_reset_password,
                'username' : result.rows[0].username,
                'added' : result.rows[0].added,
                'updated' : result.rows[0].updated,
                'count_subscribe_to' : result.rows[0].count_subscribe_to,
                'count_subscribe_by' : result.rows[0].count_subscribe_by,
            });
        }
    })
});

app.get('/user/like/:username', (req, res) => {

    console.log("[GET] /user/like/" + req.params.username);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query('SELECT *, (SELECT COUNT(*) FROM PUBLIC.subscribe WHERE id_user = u.id) AS count_subscribe_to, (SELECT COUNT(*) FROM PUBLIC.subscribe WHERE id_subscribe = u.id) AS count_subscribe_by FROM PUBLIC.user u WHERE upper(username) LIKE upper($1)', ['%' + req.params.username + '%'], async(error, result) => {

        let likeUsers = [];

        result.rows.forEach(function(element) {
            likeUsers.push({
                'id' : element.id,
                'biography' : element.biography,
                'boolean_reset_password' : element.boolean_reset_password,
                'first_name' : element.first_name,
                'last_name' : element.last_name,
                'link_image_profile' : element.link_image_profile,
                'link_reset_password' : element.link_reset_password,
                'username' : element.username,
                'added' : element.added,
                'updated' : element.updated,
                'count_subscribe_to' : element.count_subscribe_to,
                'count_subscribe_by' : element.count_subscribe_by,
            })
        })

        res.send(likeUsers);
    })
});

app.get('/user/issubscribeto/:id_user/:id_subscribe', (req, res) => {

    console.log("[GET] /user/issubscribeto/" + req.params.id_user + "/" + req.params.id_subscribe);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query('SELECT * FROM PUBLIC.subscribe WHERE id_user = $1 AND id_subscribe = $2', [req.params.id_user, req.params.id_subscribe], async(error, result) => {

        let isSub = (result.rowCount === 1) ? true : false;

        res.send(isSub);
    })
});

app.get('/user/subscribeto/:id/:first/:number', (req, res) => {

    console.log("[GET] /user/subscribeto/" + req.params.id + '/' + req.params.first + '/' + req.params.number);

    res.header('Access-Control-Allow-Origin', '*');

    let queryGET = 'SELECT * FROM PUBLIC.user WHERE id IN (SELECT id_subscribe FROM PUBLIC.subscribe WHERE id_user = $1 ORDER BY added ASC LIMIT $2 OFFSET $3)';
    let paramsGET = [req.params.id, req.params.number, req.params.first];

    pool.query(queryGET, paramsGET, (error, result) => {
        if (result.rows.length < 1) {
            res.status(404).send({ error: "There is no more subscription for this user" });
        }else{
            let subscribeTo = [];

            result.rows.forEach(function(element) {
                subscribeTo.push({
                    'id' : element.id,
                    'biography' : element.biography,
                    'boolean_reset_password' : element.boolean_reset_password,
                    'first_name' : element.first_name,
                    'last_name' : element.last_name,
                    'link_image_profile' : element.link_image_profile,
                    'link_reset_password' : element.link_reset_password,
                    'username' : element.username,
                    'added' : element.added,
                    'updated' : element.updated,
                })
            })

            res.send(subscribeTo);
        }
    })
})

app.get('/user/subscribeby/:id/:first/:number', (req, res) => {

    console.log("[GET] /user/subscribeby/" + req.params.id + '/' + req.params.first + '/' + req.params.number);

    res.header('Access-Control-Allow-Origin', '*');

    let queryGET = 'SELECT * FROM PUBLIC.user WHERE id IN (SELECT id_user FROM PUBLIC.subscribe WHERE id_subscribe = $1 ORDER BY added ASC LIMIT $2 OFFSET $3)';
    let paramsGET = [req.params.id, req.params.number, req.params.first];

    pool.query(queryGET, paramsGET, (error, result) => {
        if (result.rows.length < 1) {
            res.status(404).send({ error: "There is no more subscriber for this user" });
        }else{
            let subscribeBy = [];

            result.rows.forEach(function(element) {
                subscribeBy.push({
                    'id' : element.id,
                    'biography' : element.biography,
                    'boolean_reset_password' : element.boolean_reset_password,
                    'first_name' : element.first_name,
                    'last_name' : element.last_name,
                    'link_image_profile' : element.link_image_profile,
                    'link_reset_password' : element.link_reset_password,
                    'username' : element.username,
                    'added' : element.added,
                    'updated' : element.updated,
                })
            })

            res.send(subscribeBy);
        }
    })
})

app.post('/user', async(req, res) => {

    console.log("[POST] /user");

    res.header('Access-Control-Allow-Origin', '*');

    const { first_name, last_name, username, added } = req.body;

    let accountValid = false;

    await new Promise((resolve, reject) => {
        pool.query('SELECT COUNT(*) FROM PUBLIC.user WHERE upper(username) LIKE upper($1)', [username], (error, result) => {
            resolve(result);
        });
    }).then((result) => {
        accountValid = (result.rows[0].count == 0) ? true : false;
    });

    if(!accountValid){
        res.status(409).send({ error: "There is already a user with this username" });
    }else {
        let uuidValid = false;
        let uuid = uuidv4();

        while (!uuidValid) {
            await new Promise((resolve, reject) => {
                pool.query('SELECT COUNT(*) FROM PUBLIC.user WHERE id = $1', [uuid], (error, result) => {
                    resolve(result);
                });
            }).then((result) => {
                uuidValid = (result.rows[0].count == 0) ? true : false;
                uuid = (uuidValid == true) ? uuid : uuidv4();
            });
        }

        let link_image;
        if(req.files !== null && typeof(req.files) !== "undefined" && req.files.image !== null){
            const { headers, files } = req;
            const { data, name } = files.image;

            const formData = new FormData();
            formData.append('name', 'user-' + uuid)
            formData.append('content', data, name);

            const resSub = await axios.post(config.API_IMAGE_URL, formData, {
                headers: formData.getHeaders()
            });

            link_image = resSub.data.url;
        }else{
            link_image = "https://komz-image.s3.fr-par.scw.cloud/user-default.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=SCWNHD201W5W5TP50N0B%2F20210221%2Ffr-par%2Fs3%2Faws4_request&X-Amz-Date=20210221T201302Z&X-Amz-Expires=3593&X-Amz-Signature=603051e8a179b75d7859fddec319e36b93139bc185ba6f5e094f0b736682a5d3&X-Amz-SignedHeaders=host";
        }

        console.log(link_image);

        let queryPOST = 'INSERT INTO PUBLIC.user (id, biography, boolean_reset_password, first_name, last_name, link_image_profile, username, added, updated) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)';
        let paramsPOST = [uuid, '', 0, first_name, last_name, link_image, username, added, added];

        pool.query(queryPOST, paramsPOST, (error, result) => {
            if (error) {
                res.status(400).send({ error: "An error has been thrown during the insert of the user's profile" });
            } else {
                if(result.rowCount === 0){
                    res.status(404).send({ error: "The insert of the user's profile has failed" });
                }else{
                    res.status(200).send({ message: "User's profile has been created" });
                }
            }
        })
    }
});

app.post('/user/subscribe', async(req, res) => {

    console.log("[POST] /user/subscribe");

    res.header('Access-Control-Allow-Origin', '*');

    const { id_user, id_subscribe, added } = req.body;

    let uuidValid = false;
    let uuid = uuidv4();

    while (!uuidValid) {
        await new Promise((resolve, reject) => {
            pool.query('SELECT COUNT(*) FROM PUBLIC.subscribe WHERE id = $1', [uuid], (error, result) => {
                resolve(result);
            });
        }).then((result) => {
            uuidValid = (result.rows[0].count == 0) ? true : false;
            uuid = (uuidValid == true) ? uuid : uuidv4();
        });
    }

    let queryPOST = 'INSERT INTO PUBLIC.subscribe (id, id_user, id_subscribe, added) VALUES ($1, $2, $3, $4)';
    let paramsPOST = [uuid, id_user, id_subscribe, added];

    pool.query(queryPOST, paramsPOST, (error, result) => {
        if (error) {
            res.status(400).send({ error: "An error has been thrown during the insert of the subscription" });
        } else {
            if(result.rowCount === 0){
                res.status(404).send({ error: "The insert of the subscription has failed" });
            }else{
                res.status(200).send({ message: "Subscription has been created" });
            }
        }
    })

});

app.put('/user', async(req, res) => {

    console.log("[PUT] /user");

    res.header('Access-Control-Allow-Origin', '*');

    const { id, biography, first_name, last_name, updated } = req.body;

    let idValid;

    await new Promise((resolve, reject) => {
        pool.query('SELECT * FROM PUBLIC.user WHERE id = $1', [id], (error, result) => {
            resolve(result);
        });
    }).then((result) => {
        idValid = (result.rowCount === 1) ? true : false;
    });

    if(idValid === true){

        let link_image;
        let queryPUT;
        let paramsPUT;

        if(req.files !== null && typeof(req.files) !== "undefined" && req.files.image !== null){
            const { headers, files } = req;
            const { data, name } = files.image;

            const formData = new FormData();
            formData.append('name', 'user-' + id)
            formData.append('content', data, name);

            const resSub = await axios.post(config.API_IMAGE_URL, formData, {
                headers: formData.getHeaders()
            });

            link_image = resSub.data.url;
        
            queryPUT = 'UPDATE PUBLIC.user SET biography = $1, first_name = $2, last_name = $3, link_image_profile = $4, updated = $5 WHERE id = $6';
            paramsPUT = [biography, first_name, last_name, link_image, updated, id];
        }else{
            queryPUT = 'UPDATE PUBLIC.user SET biography = $1, first_name = $2, last_name = $3, updated = $4 WHERE id = $5';
            paramsPUT = [biography, first_name, last_name, updated, id];
        }

        pool.query(queryPUT, paramsPUT, (error, result) => {
            if(error){
                res.status(400).send({ error: "An error has been thrown during the update of the user's profile" });
            }else{
                if(result.rowCount === 0){
                    res.status(404).send({ error: "The update of the user's profile has failed" });
                }else {
                    res.status(200).send({ message: "User's profile has been updated" });
                }
            }
        })
    }else{
        res.status(400).send({ error: "An error has been thrown during the update of the user's profile" });
    }
});

app.delete('/user/:id', async(req, res) => {
    
    console.log("[DELETE] /user/" + req.params.id);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query("DELETE FROM PUBLIC.user WHERE id = $1", [req.params.id], (error, result) => {
        if(error){
            res.status(400).send({ error: "An error has been thrown during the delete of the user profile" });
        }else{
            if(result.rowCount === 0){
                res.status(404).send({ error: "The delete of the user's profile has failed" });
            }else {
                res.status(200).send({ message: "User's profile has been deleted" });
            }
        }
    })
})

app.delete('/user/subscribe/:id_user/:id_subscribe', async(req, res) => {

    console.log("[DELETE] /user/subscribe/" + req.params.id_user + "/" + req.params.id_subscribe);

    res.header('Access-Control-Allow-Origin', '*');

    pool.query("DELETE FROM PUBLIC.subscribe WHERE id_user = $1 AND id_subscribe = $2", [req.params.id_user, req.params.id_subscribe], (error, result) => {
        if(error){
            res.status(400).send({ error: "An error has been thrown during the delete of the subscription" });
        }else{
            if(result.rowCount === 0){
                res.status(404).send({ error: "The delete of the subscription has failed" });
            }else {
                res.status(200).send({ message: "Subscription has been deleted" });
            }
        }
    })
})

app.listen(8080, () => {
    console.log("Server listening on port 8080...");
})
