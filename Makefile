clean:
	docker image rm komz-api-user:latest node:10-alpine sr3thore/komz-api-user:latest
install:
	docker build -t sr3thore/komz-api-user . && \
	docker-compose build && \
	npm install
ip:
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' komz-api-user
logs:
	docker-compose logs -f api
start:
	docker-compose up -d
stop:
	docker-compose down
